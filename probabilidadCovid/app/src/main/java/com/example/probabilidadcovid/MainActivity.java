package com.example.probabilidadcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    double calcular = 0, edad = 0;
    private TextView result;
    private RadioButton rbtnFemenino, rbtnMasculino;
    private RadioButton rbtnPbajo, rbtnPnormal, rbtnSobrepeso, rbtnObesidad;
    private EditText txtEdad;
    private CheckBox cbHipertencion, cbDiabetes, cbPulmonar, cbRenal, cbSupresion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result=(TextView)findViewById(R.id.txtResultado);
        rbtnFemenino=(RadioButton)findViewById(R.id.rbtnFemenino);
        rbtnMasculino=(RadioButton)findViewById(R.id.rbtnMasculino);

        txtEdad=(EditText)findViewById(R.id.txtEdad);

        rbtnPbajo=(RadioButton)findViewById(R.id.rbtnPbajo);
        rbtnPnormal=(RadioButton)findViewById(R.id.rbtnPnormal);
        rbtnSobrepeso=(RadioButton)findViewById(R.id.rbtnSobrepeso);
        rbtnObesidad=(RadioButton)findViewById(R.id.rbtnObesidad);

        cbHipertencion=(CheckBox)findViewById(R.id.cbHipertencion);
        cbDiabetes=(CheckBox)findViewById(R.id.cbDiabetes);
        cbPulmonar=(CheckBox)findViewById(R.id.cbPulmonar);
        cbRenal=(CheckBox)findViewById(R.id.cbRenal);
        cbSupresion=(CheckBox)findViewById(R.id.cbSupresion);


    }
    public void resultado(View view) {
        String anio=txtEdad.getText().toString();
        double year=Integer.parseInt(anio);
        calcular = 0;
        if (rbtnFemenino.isChecked()==true) {
            calcular +=1;
        } else if (rbtnMasculino.isChecked()==true) {
                calcular +=2;
            }

        calcular += year*0.8;

        if(rbtnPbajo.isChecked()==true || rbtnPnormal.isChecked()==true){
            calcular +=1;
        }else if(rbtnSobrepeso.isChecked()==true || rbtnObesidad.isChecked()==true){
                calcular +=2;
            }

        if(cbHipertencion.isChecked()==true){
            calcular +=2.5;
        }if(cbDiabetes.isChecked()==true){
            calcular +=2.5;
        }if(cbPulmonar.isChecked()==true){
            calcular +=2.5;
        }if(cbRenal.isChecked()==true){
            calcular +=6;
        }if(cbSupresion.isChecked()==true){
            calcular +=2.5;
        }

        Double resu = Double.valueOf(calcular);

        if(resu >= 0 && resu <= 25){
            result.setText("Probabilidad baja " + resu + "%");
        }else if(resu >= 26 && resu <= 50){
            result.setText("Probabilidad Media " + resu + "%");
        }else if(resu >= 51){
            result.setText("Probabilidad Alta " + resu + "%");
        }
    }
}